"""
    Conexión a SQLServer con Python
    Ejemplo de CRUD evitando inyecciones SQL
    
    @author parzibyte
    Más tutoriales en:
                        parzibyte.me/blog
"""
from bd import conexion

try:
    with conexion.cursor() as cursor:
        # En este caso no necesitamos limpiar ningún dato
        cursor.execute("SELECT Nombre, Apellido, Rut FROM  tbl_Usuario;")

        # Con fetchall traemos todas las filas
        usuarios = cursor.fetchall()

        # Recorrer e imprimir
        for usuario in usuarios:
            print(usuario)
except Exception as e:
    print("Ocurrió un error al consultar: ", e)
finally:
    conexion.close()