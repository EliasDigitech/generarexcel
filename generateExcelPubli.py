
from bd import conexion
import xlsxwriter
import shutil

try:
    with conexion.cursor() as cursor:
        cursor.execute("SELECT rut  FROM tbl_pivot_total_2019 where  (YEAR(FEC_CASTIT) = 2019) AND (PUBLICACIO>0);")

        # Con fetchall traemos todas las filas
        ruts = cursor.fetchall()
        for rut in ruts:
            print("rutString "+rut[0])
            # nameExcel = rut[0] +".xlsx"
            nameExcel = "publicacion.xlsx"
            nameExcelRuta = rut[0] 
            
            try:
                with conexion.cursor() as cursorFono: 
                    cursorFono.execute("SELECT  Rutdv, Monto, FVcto, FIngre, Fbaja  FROM   Publicacion2019 where rutdv='"+rut[0]+"';") 
                    ges = cursorFono.fetchall()
                    # print(ges)

                    # Create a workbook and add a worksheet.
                    workbook = xlsxwriter.Workbook(nameExcel)
                    worksheet = workbook.add_worksheet()

                    # Start from the first cell. Rows and columns are zero indexed.
                    row = 0
                    col = 0
                    # Iterate over the data and write it out row by row.
                    worksheet.write(row, col, "Rutdv")
                    worksheet.write(row, col + 1, "Monto" )
                    worksheet.write(row, col + 2, "FVcto")
                    worksheet.write(row, col + 3, "FIngre")
                    worksheet.write(row, col + 4, "Fbaja")
                    row = 1

                    for   ge in (ges):
                        worksheet.write(row, col, ge[0])
                        worksheet.write(row, col + 1, ge[1])
                        worksheet.write(row, col + 2, ge[2])
                        worksheet.write(row, col + 3, ge[3])
                        worksheet.write(row, col + 4, ge[4])
                        row += 1
                    workbook.close()
                    # shutil.move(nameExcel, "Excel/"+nameExcel)
                    shutil.move(nameExcel, "E:/Castigo2019/"+nameExcelRuta+"/"+nameExcel)
                
                with conexion.cursor() as cursor:
                    cursor.execute("update tbl_pivot_total_2019 set marca='ok' where  (YEAR(FEC_CASTIT) = 2019) and rut='"+rut[0]+"' ;")
            
            except Exception as e:
                print("Ocurrió un error al consultar Gestiones: ", e)
            # finally:
            #     conexion.close()

except Exception as e:
    print("Ocurrió un error al consultar: ", e)
finally:
    conexion.close()


